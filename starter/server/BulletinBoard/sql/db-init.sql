-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password, first_name, last_name) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password, first_name, last_name) values 
	('petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic');

-- insert authorities
insert into security_authority (name) values ('ROLE_ADMIN'); -- super user
insert into security_authority (name) values ('ROLE_USER'); -- normal user

-- insert mappings between users and authorities
insert into security_user_authority (user_id, authority_id) values (1, 1); -- admin has ROLE_ADMIN
insert into security_user_authority (user_id, authority_id) values (1, 2); -- admin has ROLE_USER too
insert into security_user_authority (user_id, authority_id) values (2, 2); -- petar has ROLE_USER


-- insert tipovi
insert into tip (id, naziv_tipa) values (1, 'Obavestenje');
insert into tip (id, naziv_tipa) values (2, 'Cestitka');
insert into tip (id, naziv_tipa) values (3, 'Poziv');

-- insert grupe
insert into grupa (id, naziv_grupe) values (1, 'Grupa 1. sprat');
insert into grupa (id, naziv_grupe) values (2, 'Grupa 2. sprat');
insert into grupa (id, naziv_grupe) values (3, 'Grupa 3. sprat');
insert into grupa (id, naziv_grupe) values (4, 'Grupa 4. sprat');
insert into grupa (id, naziv_grupe) values (5, 'Grupa 5. sprat');

-- inser objava for each tip
insert into objava (id, datum_objave, text_objave, korisnik_id, tip_id) values (1, '2019-06-16', 'Proslava 18. rodjendana veceras na 4. spratu. Svi su pozvani.', 2, 3);
insert into objava (id, datum_objave, text_objave, korisnik_id, tip_id) values (2, '2019-06-10', 'Obavestenje: Danas od 12h do 14h je najavljen nestanak struje u nasoj zgradi.', 1, 1);
insert into objava (id, datum_objave, text_objave, korisnik_id, tip_id) values (3, '2019-06-10', 'Cestitka: Komsiji sa 1. sprata uspesan zavrsetak fakulteta.', 2, 2);

