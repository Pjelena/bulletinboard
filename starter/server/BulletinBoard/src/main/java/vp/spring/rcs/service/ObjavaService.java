package vp.spring.rcs.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


import vp.spring.rcs.data.ObjavaRepository;
import vp.spring.rcs.model.Objava;

@Service
public class ObjavaService {

	@Autowired
	ObjavaRepository objavaRepository;


	public List<Objava> findAll() {

		return objavaRepository.findAll();
	}


	public Objava save(Objava objava) {
		return objavaRepository.save(objava);
	}


	public Objava findOne(Long id) {
		return objavaRepository.findOne(id);

	}


	public void remove(Long id) {
		objavaRepository.delete(id);
		
	}
	
	
}
