package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Objava;

public interface ObjavaRepository extends JpaRepository<Objava, Long> {
	

}
