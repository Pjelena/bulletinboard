package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Grupa;

public interface GrupaRepository extends JpaRepository<Grupa, Long> {

}
