package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Poruka;

public interface PorukaRepository extends JpaRepository<Poruka, Long>{

}
