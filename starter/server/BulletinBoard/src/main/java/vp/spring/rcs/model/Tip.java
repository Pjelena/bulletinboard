package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(catalog = "dbbulletinboard")
public class Tip {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nazivTipa;
	
	@OneToMany(mappedBy = "tip", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Objava> objave = new HashSet<>();

	public Tip(Long id, String nazivTipa, Set<Objava> objave) {
		super();
		this.id = id;
		this.nazivTipa = nazivTipa;
		this.objave = objave;
	}

	public Tip() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivTipa() {
		return nazivTipa;
	}

	public void setNazivTipa(String nazivTipa) {
		this.nazivTipa = nazivTipa;
	}

	public Set<Objava> getObjave() {
		return objave;
	}

	public void setObjave(Set<Objava> objave) {
		this.objave = objave;
	}
	
	
}
