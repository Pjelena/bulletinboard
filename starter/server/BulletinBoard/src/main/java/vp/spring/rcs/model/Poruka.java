package vp.spring.rcs.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog = "dbbulletinboard")
public class Poruka {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String textPoruke;
	
	private Date datumPoruke;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private SecurityUser korisnik;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Grupa grupa;

	public Poruka(Long id, String textPoruke, Date datumPoruke, SecurityUser korisnik, Grupa grupa) {
		super();
		this.id = id;
		this.textPoruke = textPoruke;
		this.datumPoruke = datumPoruke;
		this.korisnik = korisnik;
		this.grupa = grupa;
	}

	public Poruka() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTextPoruke() {
		return textPoruke;
	}

	public void setTextPoruke(String textPoruke) {
		this.textPoruke = textPoruke;
	}

	public Date getDatumPoruke() {
		return datumPoruke;
	}

	public void setDatumPoruke(Date datumPoruke) {
		this.datumPoruke = datumPoruke;
	}

	public SecurityUser getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(SecurityUser korisnik) {
		this.korisnik = korisnik;
	}

	public Grupa getGrupa() {
		return grupa;
	}

	public void setGrupa(Grupa grupa) {
		this.grupa = grupa;
	}
	
	
}
