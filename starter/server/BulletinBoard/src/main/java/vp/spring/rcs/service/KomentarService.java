package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vp.spring.rcs.data.KomentarRepository;
import vp.spring.rcs.model.Komentar;
import vp.spring.rcs.model.Objava;


@Component
public class KomentarService {
	
	@Autowired
	KomentarRepository komentarRepository;

	public List<Komentar> findAll(Objava objava) {
		return komentarRepository.findAll();
	}
}
