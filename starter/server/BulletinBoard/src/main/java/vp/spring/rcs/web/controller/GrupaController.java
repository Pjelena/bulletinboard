package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Grupa;
import vp.spring.rcs.model.Poruka;
import vp.spring.rcs.service.GrupaService;
import vp.spring.rcs.service.PorukaService;
import vp.spring.rcs.web.dto.GrupaDTO;
import vp.spring.rcs.web.dto.PorukaDTO;

@RestController
public class GrupaController {
	
	@Autowired
	GrupaService grupaService;
	
	@Autowired
	PorukaService porukaService;

	@GetMapping("/api/grupe")
	public ResponseEntity<List<GrupaDTO>> getAllGroups(){
		List<Grupa> grupe = grupaService.findAll();
		
		List<GrupaDTO> dtos = grupe.stream()
								.map(GrupaDTO::new)
								.collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@GetMapping("/api/grupe/{id}")
	public ResponseEntity<GrupaDTO> getOne(@PathVariable Long id){
		
		Grupa grupa = grupaService.findOne(id);
		
		GrupaDTO dto = new GrupaDTO(grupa);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@GetMapping("/api/grupe/{id}/poruke")
	public ResponseEntity<List<PorukaDTO>> getAllPoruke(@PathVariable Long id){
			Grupa grupa = grupaService.findOne(id);
			
			List<Poruka> poruke = porukaService.findAll(grupa);
			
			List<PorukaDTO> dtos = poruke.stream()
								.map(PorukaDTO:: new)
								.collect(Collectors.toList());
			
			return new ResponseEntity<>(dtos, HttpStatus.OK);	
		}
}
