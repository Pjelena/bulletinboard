package vp.spring.rcs.web.dto;

import java.util.Date;

import vp.spring.rcs.model.Komentar;

public class KomentarDTO {
	
	Long id;
	private String textKomentara;
	private Date datumKomentara;
	
	public KomentarDTO(Komentar dto) {
		this.id = dto.getId();
		this.textKomentara = dto.getTextKomentara();
		this.datumKomentara = dto.getDatumKomentara();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTextKomentara() {
		return textKomentara;
	}

	public void setTextKomentara(String textKomentara) {
		this.textKomentara = textKomentara;
	}

	public Date getDatumKomentara() {
		return datumKomentara;
	}

	public void setDatumKomentara(Date datumKomentara) {
		this.datumKomentara = datumKomentara;
	}
	
	
	
}
