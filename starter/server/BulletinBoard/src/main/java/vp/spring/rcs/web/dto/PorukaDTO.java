package vp.spring.rcs.web.dto;

import java.util.Date;

import vp.spring.rcs.model.Poruka;

public class PorukaDTO {

	Long id;
	private String textPoruke;
	private Date datumPoruke;
	
	public PorukaDTO(Poruka poruka) {
		this.id = poruka.getId();
		this.textPoruke = poruka.getTextPoruke();
		this.datumPoruke = poruka.getDatumPoruke();
	}

	public PorukaDTO() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTextPoruke() {
		return textPoruke;
	}

	public void setTextPoruke(String textPoruke) {
		this.textPoruke = textPoruke;
	}

	public Date getDatumPoruke() {
		return datumPoruke;
	}

	public void setDatumPoruke(Date datumPoruke) {
		this.datumPoruke = datumPoruke;
	}
	
	
	
}
