package vp.spring.rcs.web.dto;

import vp.spring.rcs.model.Grupa;

public class GrupaDTO {
	
	Long id;
	private String nazivGrupe;
	
	public GrupaDTO(Grupa grupa) {
		this.id = grupa.getId();
		this.nazivGrupe = grupa.getNazivGrupe();
	}

	
	public GrupaDTO() {
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivGrupe() {
		return nazivGrupe;
	}

	public void setNazivGrupe(String nazivGrupe) {
		this.nazivGrupe = nazivGrupe;
	}
	
	
}
