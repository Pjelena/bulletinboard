package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vp.spring.rcs.data.PorukaRepository;
import vp.spring.rcs.model.Grupa;
import vp.spring.rcs.model.Poruka;


@Component
public class PorukaService {
	
	@Autowired
	PorukaRepository porukaRepository;

	public List<Poruka> findAll(Grupa grupa) {
		return porukaRepository.findAll();
	}
}
