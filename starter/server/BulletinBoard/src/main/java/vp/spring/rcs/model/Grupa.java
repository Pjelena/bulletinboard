package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog = "dbbulletinboard")

public class Grupa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nazivGrupe;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<SecurityUser> korisnici = new HashSet<>();
	
	@OneToMany(mappedBy = "grupa", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Poruka> poruke = new HashSet<>();

	public Grupa(Long id, String nazivGrupe, Set<SecurityUser> korisnici, Set<Poruka> poruke) {
		super();
		this.id = id;
		this.nazivGrupe = nazivGrupe;
		this.korisnici = korisnici;
		this.poruke = poruke;
	}

	public Grupa() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivGrupe() {
		return nazivGrupe;
	}

	public void setNazivGrupe(String nazivGrupe) {
		this.nazivGrupe = nazivGrupe;
	}

	public Set<SecurityUser> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(Set<SecurityUser> korisnici) {
		this.korisnici = korisnici;
	}

	public Set<Poruka> getPoruke() {
		return poruke;
	}

	public void setPoruke(Set<Poruka> poruke) {
		this.poruke = poruke;
	} 
	
	
}
