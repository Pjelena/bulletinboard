package vp.spring.rcs.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog="dbbulletinboard")

public class Objava {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String textObjave;
	
	private Date datumObjave;
	
	@OneToMany(mappedBy = "objava")
	private Set<Komentar> komentari = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Tip tip;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private SecurityUser korisnik;

	public Objava(Long id, String textObjave, Date datumObjave, Set<Komentar> komentari, Tip tip,
			SecurityUser korisnik) {
		super();
		this.id = id;
		this.textObjave = textObjave;
		this.datumObjave = datumObjave;
		this.komentari = komentari;
		this.tip = tip;
		this.korisnik = korisnik;
	}

	public Objava() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTextObjave() {
		return textObjave;
	}

	public void setTextObjave(String textObjave) {
		this.textObjave = textObjave;
	}

	public Date getDatumObjave() {
		return datumObjave;
	}

	public void setDatumObjave(Date datumObjave) {
		this.datumObjave = datumObjave;
	}

	public Set<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(Set<Komentar> komentari) {
		this.komentari = komentari;
	}

	public Tip getTip() {
		return tip;
	}

	public void setTip(Tip tip) {
		this.tip = tip;
	}

	public SecurityUser getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(SecurityUser korisnik) {
		this.korisnik = korisnik;
	}
	
}
