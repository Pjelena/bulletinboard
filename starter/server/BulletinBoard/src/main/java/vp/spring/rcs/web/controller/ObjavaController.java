package vp.spring.rcs.web.controller;


import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Komentar;
import vp.spring.rcs.model.Objava;
import vp.spring.rcs.service.KomentarService;
import vp.spring.rcs.service.ObjavaService;
import vp.spring.rcs.web.dto.KomentarDTO;
import vp.spring.rcs.web.dto.ObjavaDTO;


@RestController
public class ObjavaController {
	
	@Autowired
	ObjavaService objavaService;
	
	@Autowired
	KomentarService komentarService;
	
	@GetMapping("/api/objave")
	public ResponseEntity<List<ObjavaDTO>> getAll(){
		
		List<Objava> objave = objavaService.findAll();
		
	
		List<ObjavaDTO> dtos=objave.stream()
				             .map(ObjavaDTO::new)
				             .collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
		
	}
	
	@PreAuthorize("isAuthenticated()")
	@PostMapping("/api/objave")
	public ResponseEntity<ObjavaDTO> create(ObjavaDTO createDTO){
		Objava objava = new Objava();
		objava.setTextObjave(createDTO.getTextObjave());
		objava.setDatumObjave(createDTO.getDatumObjave());
		
		objava = objavaService.save(objava);
		
		ObjavaDTO dto = new ObjavaDTO(objava);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping("/api/objave/{id}")
	public ResponseEntity<ObjavaDTO> getOne(@PathVariable Long id){
		Objava objava = objavaService.findOne(id);
		
		ObjavaDTO dto = new ObjavaDTO(objava);
		
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	//Still in progress
	@GetMapping("/api/objave/{id}/komentari")
	public ResponseEntity<List<KomentarDTO>> getAllComments(@PathVariable Long id){
		Objava objava = objavaService.findOne(id);
		
		List<Komentar> komentari = komentarService.findAll(objava);
		
		
		List<KomentarDTO> dtos = komentari.stream()
							.map(KomentarDTO::new)
							.collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}


	@DeleteMapping("/api/objave")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Objava objava = objavaService.findOne(id);
		
		if(objava!= null) {
			objavaService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
	}
}
