package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vp.spring.rcs.data.GrupaRepository;
import vp.spring.rcs.model.Grupa;

@Component
public class GrupaService {
	
	@Autowired
	GrupaRepository grupaRepository;

	public List<Grupa> findAll() {
		return grupaRepository.findAll();
	}

	public Grupa findOne(Long id) {
		return grupaRepository.findOne(id);
	}
}
