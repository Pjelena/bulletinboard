package vp.spring.rcs.model.user;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import vp.spring.rcs.model.Grupa;
import vp.spring.rcs.model.Komentar;
import vp.spring.rcs.model.Objava;
import vp.spring.rcs.model.Poruka;

@Entity
public class SecurityUser {
	@Id
	@GeneratedValue
	private Long id;
	
	private String username;
	
	private String password;
	
	private String firstName;
	
	private String lastName;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<SecurityUserAuthority> userAuthorities = new HashSet<SecurityUserAuthority>();
	
	@OneToMany(mappedBy = "korisnik", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Objava> objave = new HashSet<>();
	
	@OneToMany(mappedBy = "korisnik", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Komentar> komentari = new HashSet<>();

	@ManyToMany(mappedBy= "korisnici", fetch = FetchType.EAGER)
	private Set<Grupa> grupe = new HashSet<>();
	
	@OneToMany(mappedBy = "korisnik", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Poruka> poruke = new HashSet<>();
		
	public Set<Grupa> getGrupe() {
		return grupe;
	}

	public void setGrupe(Set<Grupa> grupe) {
		this.grupe = grupe;
	}

	public Set<Poruka> getPoruke() {
		return poruke;
	}

	public void setPoruke(Set<Poruka> poruke) {
		this.poruke = poruke;
	}

	public Set<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(Set<Komentar> komentari) {
		this.komentari = komentari;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Objava> getObjave() {
		return objave;
	}

	public void setObjave(Set<Objava> objave) {
		this.objave = objave;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<SecurityUserAuthority> getUserAuthorities() {
		return userAuthorities;
	}

	public void setUserAuthorities(Set<SecurityUserAuthority> userAuthorities) {
		this.userAuthorities = userAuthorities;
	}
}
