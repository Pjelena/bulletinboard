package vp.spring.rcs.web.dto;

import java.util.Date;

import vp.spring.rcs.model.Objava;

public class ObjavaDTO {

	Long Id;
	private String textObjave;
	private Date datumObjave;
	
	public ObjavaDTO(Objava dto) {
		this.Id = dto.getId();
		this.textObjave = dto.getTextObjave();
		this.datumObjave = dto.getDatumObjave();
	}
	
	public ObjavaDTO() {
    }
	
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getTextObjave() {
		return textObjave;
	}
	public void setTextObjave(String textObjave) {
		this.textObjave = textObjave;
	}
	public Date getDatumObjave() {
		return datumObjave;
	}
	public void setDatumObjave(Date datumObjave) {
		this.datumObjave = datumObjave;
	}
	
}
