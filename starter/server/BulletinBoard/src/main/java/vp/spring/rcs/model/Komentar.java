package vp.spring.rcs.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog = "dbbulletinboard")

public class Komentar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String textKomentara;
	
	private Date datumKomentara;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Objava objava;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private SecurityUser korisnik;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Komentar komentar;
	
	@OneToMany(mappedBy = "komentar", fetch= FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Komentar> komentari = new HashSet<>();

	public Komentar(Long id, String textKomentara, Date datumKomentara, Objava objava, SecurityUser korisnik,
			Komentar komentar, Set<Komentar> komentari) {
		super();
		this.id = id;
		this.textKomentara = textKomentara;
		this.datumKomentara = datumKomentara;
		this.objava = objava;
		this.korisnik = korisnik;
		this.komentar = komentar;
		this.komentari = komentari;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTextKomentara() {
		return textKomentara;
	}

	public void setTextKomentara(String textKomentara) {
		this.textKomentara = textKomentara;
	}

	public Date getDatumKomentara() {
		return datumKomentara;
	}

	public void setDatumKomentara(Date datumKomentara) {
		this.datumKomentara = datumKomentara;
	}

	public Objava getObjava() {
		return objava;
	}

	public void setObjava(Objava objava) {
		this.objava = objava;
	}

	public SecurityUser getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(SecurityUser korisnik) {
		this.korisnik = korisnik;
	}

	public Komentar getKomentar() {
		return komentar;
	}

	public void setKomentar(Komentar komentar) {
		this.komentar = komentar;
	}

	public Set<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(Set<Komentar> komentari) {
		this.komentari = komentari;
	}
	
	
}
