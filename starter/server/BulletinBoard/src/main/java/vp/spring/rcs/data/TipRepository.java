package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Tip;

public interface TipRepository extends JpaRepository<Tip, Long> {

}
